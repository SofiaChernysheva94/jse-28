package ru.t1.chernysheva.tm.command.data;

import lombok.SneakyThrows;
import org.antlr.runtime.debug.DebugEventSocketProxy;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.Domain;
import ru.t1.chernysheva.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @NotNull
    private static final String DESCRIPTION  = "Load data from json file - jaxb.";

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(MEDIA_FILE, APPLICATION_TYPE_JSON);
        @NotNull final File file = new File(FILE_JSON);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

    @Override
    public String getArgument() {
        return null;
    }

}
