package ru.t1.chernysheva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Role;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "exit";

    @NotNull
    private final String DESCRIPTION = "Close application.";

    @Override
    public void execute() {
        System.exit(0);
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
